//
//  ColorScheme.swift
//  Beers-Of-Sydney
//
//  Created by Cecilia Hollins on 4/7/21.
//

import Foundation
import UIKit

public struct ColorAssets {
    
    public var primary: UIColor {
        return UIColor(named: "lightBlue")!
    }
    
    public var secondary: UIColor {
        return UIColor(named: "orange")!
    }
    
    public var teritary: UIColor {
        return UIColor(named: "pink")!
    }
    
    public var neutralDark: UIColor {
        return UIColor(named: "neutralDark")!
    }
    
    public var neutralLight: UIColor {
        return UIColor(named: "neutralLight")!
    }
}
