//
//  FontScheme.swift
//  Beers-Of-Sydney
//
//  Created by Cecilia Hollins on 4/7/21.
//

import Foundation
import UIKit

public struct FontAssets {
    
    public var h1: UIFont {
        UIFont(name: "Propaganda", size: 30)!
    }
}
